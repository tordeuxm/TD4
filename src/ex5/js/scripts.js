const xhr = new XMLHttpRequest();
const div_verbes = document.getElementById("liste_verbes");
const div_input = document.getElementById("input");
const alpha = "abcdefghijklmnopqrstuvzœ";


function creer_interface() {
    div_input.innerHTML = "<input id='seq' type='text' placeholder='entrez une séquence'>";
    for (let i = 0; i < 24; i++) {
        div_input.innerHTML += `<input type='button' class='letter' value=${alpha[i]}>`;
    }
    div_input.innerHTML += "<input type='button' class='erase' value='effacer la liste'>";

    let div_lettre = document.getElementsByClassName('letter');
    for (let i = 0; i < 24; i++) {
        div_lettre[i].addEventListener('click', function () {
            charger_verbes(this.value, 'init');
        })
    }
    let div_text = document.getElementById("seq");
    div_text.addEventListener('change', function () {
        charger_verbes(this.value, 'seq');
    })

    let erase = document.getElementsByClassName('erase');
    erase[0].addEventListener('click', function () {
        div_verbes.innerHTML = "";
    })

}

function callback_basique() {
    let xhrJSON = JSON.parse(xhr.responseText);
    console.log(xhrJSON);
}

function callback() {
    div_verbes.innerHTML = "";
    let xhrJSON = JSON.parse(xhr.responseText);
    console.log(xhrJSON);
    for (let i = 0; i < xhrJSON.length; i++) {
        div_verbes.innerHTML += `<p>${xhrJSON[i].libelle}</p>`;
    }
}

/**
 *
 * @param {String} lettre Chaîne recherchée dans les verbes
 * @param {String} type Type de recherche ('seq' ou 'init')
 */
function charger_verbes(lettre, type) {
    const url = `https://webinfo.iutmontp.univ-montp2.fr/~tordeuxm/JS/TD/TD4/src/ex5/php/recherche.php?lettre=${lettre}&type=${type}`;
    xhr.open("GET", url, true);
    xhr.addEventListener("load", callback);
    xhr.send(null);

}


// Création de l'interface
creer_interface();
